/*
 * Firmware updateClient for ESP
 * (c) 2018 by Hartmut Eilers <hartmut@eilers.net>
 *
 * this client connects to a Hawkbit server, checks for available updates
 * if updates of the firmware are available the client downloads and
 * installs these updates
 *
 */

#include "updateClient.h"
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>

  updateClient::updateClient(char Host[],char Tenant[],char Token[]) {
    // eg updateClient FBUpdate("device.eu1.bosch-iot-rollouts.com","2A553CA5-6A8C-4E49-993A-F0C8D47B0414","b7d0807a9cc18af19b0158f1a5dafABC");
    // example URL: https://device.eu1.bosch-iot-rollouts.com/2A553CA5-6A8C-4E49-993A-F0C8D47B0414/controller/v1
    // this is the base URL for the communication with the hawkbit update server
    strcpy(_URL,"https://" + Host + "/" + Tenant + "/controller/v1");
    strcpy(_Host,Host);
    strcpy(_Tenant,Tenant);
    strcpy(_Token,Token);
  }

  int updateClient::check() {
    int httpError;

    HTTPClient checkUpdate;

    checkUpdate.begin(_URL);
    // set the headers here!
    int httpCode = checkUpdate.GET();
    checkUpdate.sendHeader("Host:", _Host);
    checkUpdate.sendHeader("TargetToken", _Token);
    if(httpCode == HTTP_CODE_OK) {
      // parse the JSON response
      StaticJsonBuffer<1000> jsonBuffer;
      String payload = checkUpdate.getString();
      JsonObject& reply = jsonBuffer.parseObject(payload);
      if ( length(reply["href"]) > 0 ) {                                        // a href in the reply signals an outstanding update
        // get the details for the updates
        HTTPClient getDetails;
        getDetails.begin(reply["href"]);
        // set the headers here!
        httpCode = getDetails.GET();
        getDetails.sendHeader("Host:", _Host);
        getDetails.sendHeader("TargetToken", _Token);
        if (httpCode == HTTP_CODE_OK) {
          payload = checkUpdate.getString();
          reply = jsonBuffer.parseObject(payload);
          if ( length(reply["href"]) > 0 ) {
            // this is the URL of the firmware file
            HTTPClient downloadFW;
            downloadFW.begin(reply["href"]);
            httpCode = downloadFW.GET();
            downloadFW.sendHeader("Host:", _Host);
            downloadFW.sendHeader("TargetToken", _Token);
            //downloadFW.sendBody();
            // dump the firmware to serial port for debugging
            // ToDo: write stream to file on SPIFFS
            payload = downloadFW.writeToStream(&Serial);
            downloadFW.end();
            // ToDo: install FW from SPIFFS
            httpError=0;
          } else {
            Serial.println("ERROR: weird, failure retrieving FW download URL");
            httpError=-1;
          }
          getDetails.end();
        } else {
          Serial.println("ERROR: response to call " + reply["href"] + " was " + httpCode);
          httpError=-2;
        }
      } else {
        Serial.println("No Updates available");
        // no update available, get the polling interval to know when to ask again
        String repolltime = reply["sleep"];
        httpError=1;
      }
    } else {
      Serial.println("ERROR: response to call " + _URL + " was " + httpCode);
      httpError=-3;
    }
    checkUpdate.end();

    // ToDo: provide feedback to HawkBit
    if ( httpError == 0 ) {
      // success
    } else  {
      if ( httpError < 0 ) {
        // failure
      } else {
        // no new firmware -> nothing to do
        Serial.println('No FW updates available, checking back later...')
      }
    }

    return httpError;
  }
