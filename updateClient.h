/*
 * Firmware updateClient for ESP
 * (c) 2018 by Hartmut Eilers <hartmut@eilers.net>
 *
 * this client connects to a Hawkbit server, checks for available updates
 * if updates of the firmware are available the client downloads and
 * installs these updates
 *
 */

#ifndef updateClient_h
#define updateClient_h

#include <string.h>

class updateClient {
  public:
    updateClient(char Host[],char Tenant[],char Token[]]);
    int check();                    // checks wether an update is available
  private:
    char _URL[];
    char _Host[];
    char _Tenant[];
    char _Token[];
};

#endif
