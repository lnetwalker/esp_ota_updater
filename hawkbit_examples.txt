HawkBit Update Server, examples of a query sequence with Update


1) ask hawkbit for state:   ( reply nothing to do )

curl 'https://device.eu1.bosch-iot-rollouts.com/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/controller/v1/ESP08154711' -i -H 'Accept: application/hal+json' -H 'Host: device.eu1.bosch-iot-rollouts.com' -H 'Authorization: TargetToken b7d0807a9cc18af19b0158f1a5daf63c'
HTTP/1.1 200 OK
Expires: 0
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
X-XSS-Protection: 1; mode=block
Server: ddi-server
Pragma: no-cache
X-Frame-Options: DENY
Date: Thu, 20 Sep 2018 19:58:02 GMT
Connection: keep-alive
X-Content-Type-Options: nosniff
Strict-Transport-Security: max-age=31536000 ; includeSubDomains
Content-Type: application/hal+json;charset=UTF-8
Content-Length: 198
X-Application-Context: ddi-server:aws,docker:4add839b-2600-4bdc-86bd-c117197eeba0

{
  "config":
  {
    "polling":
    {
      "sleep":"00:05:00"
    }
  },
  "_links":
  {
    "configData":
    {
      "href":"https://device.eu1.bosch-iot-rollouts.com:443/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/controller/v1/ESP08154711/configData"
    }
  }
}


2) ask hawkbit for state:    ( reply with possible update )


curl 'https://device.eu1.bosch-iot-rollouts.com/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/controller/v1/ESP08154711' -i -H 'Accept: application/hal+json' -H 'device.eu1.bosch-iot-rollouts.com' -H 'Authorization: TargetToken b7d0807a9cc18af19b0158f1a5daf63c'
HTTP/1.1 200 OK
Expires: 0
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
X-XSS-Protection: 1; mode=block
Server: ddi-server
Pragma: no-cache
X-Frame-Options: DENY
Date: Thu, 20 Sep 2018 20:02:42 GMT
Connection: keep-alive
X-Content-Type-Options: nosniff
Strict-Transport-Security: max-age=31536000 ; includeSubDomains
Content-Type: application/hal+json;charset=UTF-8
Content-Length: 368
X-Application-Context: ddi-server:aws,docker:5ebb2c87-fda6-49e8-8797-92134c16a1ef

{
  "config":
  {
    "polling":
    {
      "sleep":"00:05:00"
    }
  },
  "_links":
  {
    "deploymentBase":
    {
      "href":"https://device.eu1.bosch-iot-rollouts.com:443/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/controller/v1/ESP08154711/deploymentBase/31970?c=758318209"},
      "configData":{"href":"https://device.eu1.bosch-iot-rollouts.com:443/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/controller/v1/ESP08154711/configData"}
    }
  }


3 ) ask hawkbit for the updatedetails after 2) has signaled available update


curl 'https://device.eu1.bosch-iot-rollouts.com:443/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/controller/v1/ESP08154711/deploymentBase/31970?c=758318209' -i -H 'Accept: application/hal+json' -H 'device.eu1.bosch-iot-rollouts.com' -H 'Authorization: TargetToken b7d0807a9cc18af19b0158f1a5daf63c'
HTTP/1.1 200 OK
Expires: 0
Cache-Control: no-cache, no-store, max-age=0, must-revalidate
X-XSS-Protection: 1; mode=block
Server: ddi-server
Pragma: no-cache
X-Frame-Options: DENY
Date: Fri, 21 Sep 2018 06:36:15 GMT
Connection: keep-alive
X-Content-Type-Options: nosniff
Strict-Transport-Security: max-age=31536000 ; includeSubDomains
Content-Type: application/hal+json;charset=UTF-8
Content-Length: 855
X-Application-Context: ddi-server:aws,docker:5ebb2c87-fda6-49e8-8797-92134c16a1ef

  {
    "id":"31970",
    "deployment":
    {
      "download":"attempt","update":"attempt","chunks":
      [
        {
          "part":"os","version":"0.1",
          "name":"GraphicLines","artifacts":
          [
            {
              "filename":"GraphicLines.ino.bin",
              "hashes":
              {
                "sha1":"737771e651cb01e6b6dc268cbd612a05adfa9e00",
                "md5":"9162bfc8413064138910d137a656576f"
              },
              "size":261792,
              "_links":
              {
                "download":
                {
                  "href":"https://cdn.eu1.bosch-iot-rollouts.com/2A553CA5-6A8C-4E49-993A-F0C8D47B0415/737771e651cb01e6b6dc268cbd612a05adfa9e00?Expires=1540103775&Signature=RCJv43yrAHhzPgEQI6KjUY5NcJa8zGa217vfTiFq19TyfgNG-u~81l5SEDK2W~tJLJXRPUsrKMnpL-PceZqGXSmuP4Y1531js30xXHptEDJ8Zl4-X9-WkCJPSl521o0VkUD7727IaKnxa2-cRFfhaA02tFLMDlRjO2wN6pduKLoyfdl9MyY1-0RK2RqPuz~7N2-YL~4o2yCIC2luhUkMs2RyGOdkLqSJHcmxrbjQM~NdVe2XyzOYfB3YsYjuu9Mn8rmv0lWNmbHRfBUKpJNOUgmUYqjK6Po0HMSw6RYJzvADUXpHOz74GaDxGomvzOIbCR-w92L0EsTPhHpsX3oJ2g__&Key-Pair-Id=APKAJ7V55VK3Y2WFZOHQ"
                }
              }
            }
          ]
        }
      ]
    }
  }


4) now download file using download href url



5) give feedback with:

POST /{tenant}/controller/v1/{controllerid}/deploymentBase/{actionId}/feedback
